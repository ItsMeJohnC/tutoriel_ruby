# La navigation sûre

Depuis Ruby 2.3.0 il existe un opérateur permettant de simplifier les opérations de "navigation". Cet opérateur est l'opérateur `&.`.

## Qu'est-ce que la navigation ?

La navigation, c'est simplement le fait d'aller chercher des propriétés d'un objet ou de ses sous objets. Dans certains cas, les sous-objets ou la variable qui devraient contenir l'objet ne sont pas initialisés (ça vaut `nil`), pour éviter les erreurs nous avons l'habitude d'écrire ceci :
```ruby
if variable && variable.propriete
  # faire quelque chose avec variable.propriete
end
```
On peut également écrire de manière optimisée :
```ruby
if (propriete = variable && variable.propriete)
  # faire quelque chose avec prioriete
end
```

Ceci permet soit d'appeler des méthodes soit d'exploiter les propriétés de l'objet en question.

## La navigation sûre

La navigation sûre est un principe similaire à ce qui est décrit au dessus, mais à la différence que l'appel de la méthode est réalisé si et seulement si l'objet n'est pas `nil`.

Les exemples du dessus peuvent s'écrire ainsi avec l'opérateur `&.` :
```ruby
if variable&.propriete
  # faire quelque chose avec variable.propriete
end
```
Ou bien :
```ruby
if (propriete = variable&.propriete)
  # Faire quelque chose avec propriete
end
```

L'avantage de cet opérateur par rapport à `&&` est que la valeur `false` peut voir ses propriétés ou méthodes appelées.
Exemple :
```ruby
str = variable&.to_s
```
Si variable vaut `nil` str contiendra `nil`, par contre si la variable vaut `false` str contiendra `"false"`. La méthode `to_s` de FalseClass aura été appelée.

## Sources

Cette page provient d'une recherche qui n'avait rien avoir avec le contenu ($SAFE level) mais la magie des moteur de recherche a fait que je suis tombé par hasard sur un article qui parle de ça:
[The Safe Navigation Operator (&.) in Ruby](http://mitrev.net/ruby/2015/11/13/the-operator-in-ruby/) par Georgi Mitrev.

Je vous conseille fortement de lire la source, elle est plus détaillée que cette page :)