# Les tableaux ordonnés
Les tableaux ordonnés ou Array sont des objets permettant d'accumuler n'importe quel autre type d'objet. En Ruby ils ont plusieurs utilités : 
* Décrire simplement une liste d'éléments
* Accumuler des paramètres dynamiques d'une méthode
* Réaliser des conversions

## Les bases

Dans la partie [Variable et types de données](1_Variable_Et_Type_De_Donnes.md) nous avons vu comment définir un tableau de manière littérale. Sachez qu'il existe d'autres manières de définir des tableaux.

### Créer un tableau dynamiquement

Parfois, vous aurez besoin de définir un tableau contenant déjà des éléments ou ayant une taille fixe sans avoir à l'écrire en entier. Pour se faire, on utilise la méthode `new` de la classe Array. (Pour comprendre certains principes de fonctionnement, je vous renvoie à la partie [Les méthodes et les blocs](6_Methodes_Et_Bloc.md)).

#### Créer un tableau d'une taille fixe

Le premier paramètre de la méthode `new` de la classe Array est la taille du tableau, si vous ne donnez que ce paramètre cela crée un tableau contenant des éléments néant (`nil`) de la taille spécifiée.

Exemple :
```ruby
Array.new(5) # [nil, nil, nil, nil, nil]
```

#### Créer un tableau contenant un élément spécifié

Le deuxième paramètre optionnel de la méthode `new` permet de spécifier l'élément qui est utilisé pour remplir le tableau.

Exemple :
```ruby
Array.new(5, 0) # [0, 0, 0, 0, 0]
```

Attention, quand vous utilisez le deuxième paramètre, le tableau est rempli par l'objet donné (aucune réplication n'est faite). De ce fait, modifier le contenu de cet objet est équivalent à modifier le contenu de tous les objets du tableau vu que le tableau est exclusivement rempli de cet objet.

#### Créer un tableau contenant les éléments d'un type

Il est possible d'utiliser un bloc pour que le remplissage du tableau se fasse avec une réplication de l'objet.

Exemple :
```ruby
Array.new(2) { [] } # [[], []]
```

Modifier le premier tableau de ce tableau n'affectera pas le deuxième tableau.

#### Créer un tableau contenant des éléments calculés

Ceci vous permet d'éviter d'utiliser une boucle juste après la création du tableau en permettant d'exécuter du code directement pendant la création du tableau grâce au paramètre du bloc.

Exemple ;
```ruby
Array.new(5) do |index| # L'index commence à 0
    nombre_en_partant_de_un = index + 1
    next(nombre_en_partant_de_un * 2)
end # [2, 4, 6, 8, 10]
```
Dans cet exemple nous avons créé un tableau de nombres entiers pairs et non nuls.

Note : `next(valeur)` permet de dire explicitement que la valeur à inscrire est valeur. Le code des lignes en dessous de `next` n'est pas exécuté car `next` veut également dire "passer à l'itération suivante".

### Accéder aux éléments d'un tableau

Comme dans la plupart des langages de programmation, on peut accéder à un élément d'un tableau en utilisant l'opérateur `[]`.

Exemple :
```ruby
tableau = [3, 6, 8, 9]
tableau[0] # renvoie 3
```

#### Accéder à plusieurs éléments en même temps

En Ruby, il existe une méthode simple pour obtenir plusieurs éléments d'un tableau : `array[début, taille]` ou `array[début..fin]`.

Dans les deux cas, début indique l'indice de départ où prendre les éléments, s'il est négatif on part de la fin du tableau (-1 = dernière case, -2 = avant dernière case etc...). 

Dans le même ordre d'idée, fin peut être négatif, comme début, par contre, la taille doit toujours être positive.

Si la plage voulue est hors du tableau, vous obtiendrez `nil`.

Exemples :
```ruby
array = [0, 1, 2, 3, 4, 5]
array[2, 2] # [2, 3]
array[2..4] # [2, 3]
array[-2, 2] # [4, 5]
array[-2, 9999] # [4, 5]
array[2..-1] # [2, 3, 4, 5]
array[-3..-1] # [3, 4, 5]
array[9..10] # nil
array[-20, 1] # nil
```

### Modifier les éléments d'un tableau

Il est possible de modifier les éléments d'un tableau en utilisant la méthode `[]=`. Cette méthode fonctionne de la même manière que `[]` à la différence que les indices négatifs à l'extérieur du tableau causent un `IndexError`.

Si le point de départ se trouve après la fin du tableau, la partie entre la fin et le point de départ est remplie de `nil`.

Exemple :
```ruby
array = [0]
array[3, 2] = [1, 2]
# array = [0, nil, nil, 1, 2]
```

Notez que si la taille du tableau inséré ne correspond pas à la taille d'insertion, Ruby supprimera les valeurs manquantes dans le tableau affecté ou alors décalera les valeurs qui sont en dehors de la zone d'insertion.

Exemples :
```ruby
array = [1, 2, 3, 4, 5]
array[1, 3] = [1, 2]
# array = [1, 1, 2, 5] # le 4 a été supprimé
array = [1, 2, 3, 4, 5]
array[1, 2] = [9, 10, 11, 12]
# array = [1, 9, 10, 11, 12, 4, 5] # 4 et 5 ont été préservés.
```

### Obtenir la taille du tableau

Il existe une tripotée de cas où connaître la taille du tableau peut s'avérer utile, rien de plus compliqué, vous pouvez soit appeler la méthode `size` ou la méthode `length`.

Exemples :
```ruby
array = [0, 1, 2]
array.size # 3
array = [0, 1, 2, 9, 3, 22]
array.size # 6
```

### Ajouter des éléments dans le tableau

Pour ajouter des éléments dans le tableau il existe deux méthodes :
* `push` pour les ajouter à la fin.
* `unshift` pour les ajouter au début.

Ces deux méthodes ont un alias plus explicite : `append` et `prepend`.

Exemples : 
```ruby
array = [0]
array.push(1) # array = [0, 1]
array.push(2, 3) # array = [0, 1, 2, 3]
array.unshift(-1) # array = [-1, 0, 1, 2, 3]
array.unshift(-3, -2) # array = [-3, -2, -1, 0, 1, 2, 3]
```

### Retirer des éléments du tableau

D'une manière équivalente il existe des méthodes pour retirer des éléments du tableau :
* `pop` pour retirer les éléments de la fin
* `shift` pour retirer les éléments du début

On peut préciser le nombre d'éléments à retirer.

Exemple :
```ruby
array = [0, 1, 2, 3, 4, 5, 6, 7]
array.pop # 7
# array = [0, 1, 2, 3, 4, 5, 6]
array.pop(2) # [5, 6]
# array = [0, 1, 2, 3, 4]
array.shift # 0
# array = [1, 2, 3, 4]
array.shift(2) # [1, 2]
# array = [3, 4]
```

### Insérer des éléments au milieu du tableau

Pour insérer des éléments au milieu d'un tableau rien de plus simple, il suffit d'appeler la méthode `insert`. Les paramètres sont la position d'insertion et les éléments.

Exemple :
```ruby
array = [0, 1, 2]
array.insert(1, 9) 
# array = [0, 9, 1, 2]
array.insert(2, -2, -3, -4) 
# array = [0, 9, -2, -3, -4, 1, 2]
```

### Supprimer des éléments au milieu du tableau

A l'opposé de l'insertion d'éléments, on a la suppression d'éléments, il existe deux manières de s'y prendre. 
* Supprimer plusieurs éléments avec `slice!`.
* Supprimer un seul élément avec `delete_at`.

Les paramètres de `slice!` sont les mêmes que `[]` (`slice` est un alias de `[]` mais n'affecte pas le tableau source au contraire de `slice!` qui efface la partie prise.)

Exemples :
```ruby
array = [0, 1, 2, 3, 4, 5]
array.delete_at(3) # 3
# array = [0, 1, 2, 4, 5]
array.slice!(-3, 2) # [2, 4]
# array = [0, 1, 5]
array.slice!(1..-1) # [1, 5]
# array = [0]
```

### Vérifier l'existence d'un élément dans le tableau

En appelant la méthode `include?` il est possible de vérifier l'existence d'un élément dans un tableau. Le paramètre de `include?` est l'élément à vérifier.

Exemple :
```ruby
array = [:one, :two, :three]
array.include?(:two) # true
array.include?(:four) # false
```

### Obtenir la position d'un élément dans le tableau

Pour cela il existe deux méthodes :
* `index` qui renvoie la position du premier élément qui correspond.
* `rindex` qui renvoie la position du dernier élément qui correspond.

Selon le critère voulu (égalité de l'objet ou une propriété de l'objet) on peut donner un paramètre ou un bloc.

Notez que si l'élément n'a pas été trouvé, le retour est `nil`.

Exemples :
```ruby
array = [0, 5, 8, 0, 7, 4]
array.index(8) # 2
array.index { |element| element.odd? } # 1
array.rindex(0) # 3
array.rindex { |element| element.odd? } # 4
```

### Supprimer tous les éléments néant d'un tableau

Parfois votre tableau contient la valeur `nil`. Si vous voulez vous en débarrasser simplement, il suffit d'utiliser la méthode `compact` (crée un nouveau tableau) ou `compact!` (modifie ce tableau).

## L'exploration

Parfois, vous aurez besoin d'explorer un tableau, il existe trois méthodes qui peuvent vous servir :
* `each` : parcourt tous les éléments du tableau (paramètre de bloc = l'élément).
* `each_with_index` : même chose mais avec l'index en plus dans le paramètre du bloc.
* `each_index` : parcourt tous les index du tableau (paramètre du bloc = l'élément)

Exemples :
```ruby
array = ['1', 2, 33]
array.each { |element| p element }
# affiche :
# "1"
# 2
# 33
array.each_with_index { |element, index| puts "array[#{index}] = #{element.inspect}" }
# affiche :
# array[0] = "1"
# array[1] = 2
# array[2] = 33
array.each_index { |index| p index }
# affiche :
# 0
# 1
# 2
```

Vous choisirez l'une des méthodes selon vos besoins. Notez qu'il existe d'autres manières de parcourir les tableaux pour réaliser d'autres choses, par exemple :
* Obtenir toutes les valeurs en les modifiant légèrement : `map` ou `collect`.
* Supprimer toutes les valeurs non désirées du tableau : `reject` (nouveau tableau en retour) ou `reject!` ou `delete_if` (affecte le tableau).
* Sélectionner que les valeurs d'intérêt : `select`.
* Trouver un élément selon des critères : `find`

Il y en a toute une flopée, je vous invite à lire la partie Enumerable ou Array de la documentation.


## Mot de la fin

Il existe tout un tas de méthodes vachement utiles dans la classe Array, je n'aborderai pas tout et je verrais certaines méthodes plus tard. Je vous recommande vivement de regarder la documentation de Ruby.